import 'package:Healt4U/routes.dart';
import 'package:flutter/material.dart';

class Healt4U extends StatelessWidget {
  const Healt4U({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: routes,
      initialRoute: '/selectAuth',
    );
  }
}