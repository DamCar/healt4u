import 'package:Healt4U/healt_4_u.dart';
import 'package:Healt4U/pages/fallbacks/loading.dart';
import 'package:Healt4U/pages/fallbacks/something_went_wrong.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
 
void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(App());
}
 


class App extends StatefulWidget {
  App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        Widget render;
        switch (snapshot.connectionState) {
          case ConnectionState.done:
          if (snapshot.hasError) {
            render = const SomethingWentWrong();
          } else {
            render = const Healt4U();
          } 
          break;
          default:
          render = const Loading();
        }
        return MaterialApp(
          home: render,
        );
      },
    );
  }
}
