import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const 
    Scaffold(
      body: SizedBox.expand(
        child: SafeArea(
          child: Text('Loading...'),
        ),  
      ),
    );
  }
}