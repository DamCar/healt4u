import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Background extends StatelessWidget {
  final Widget body;
  final Widget footer;

  Background({
    Key? key,
    required this.body,
    required this.footer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size  =  MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xff38ef7d),
            Color(0xff11998E)
          ]
        ),
      ),
      child: SafeArea(
        right: true,
        left: true,
        child: Stack(
          children: [
            Column(
              children: [
                SvgPicture.asset(
                  'assets/img/Healtastyc.svg',
                  height: size.height * 0.5,
                  fit: BoxFit.scaleDown,
                )
              ],
            ),
            body,
            Positioned(
              width: size.width,
              bottom: 0,
              child: footer
            ),
          ],
        ),
      ),
    );
  }
}