import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Healt4U/pages/login/login_page.dart';
import 'package:Healt4U/pages/selectAuth/select_auth.dart';


final Map<String,WidgetBuilder> routes = {
  '/selectAuth': (context) => const SelectAuth(),
  '/login': (context) => Login()
};